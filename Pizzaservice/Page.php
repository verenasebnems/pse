<?php	 
abstract class Page
{

	protected $database = null;

	protected function __construct() {
		// activate full error checking
		error_reporting (E_ALL);

		// open database
		require_once 'pwd.php'; // read account data
        $databasen="pizzaservice";
        
		$this->database = new MySQLi($host, $user, $pwd, $databasen);
		// check connection to database
	    if (mysqli_connect_errno())
	        throw new Exception("Keine Verbindung zur Datenbank: ".mysqli_connect_error());
		// set character encoding to UTF-8
		if (!$this->database->set_charset("utf8"))
		    throw new Exception("Fehler beim Laden des Zeichensatzes UTF-8: ".$this->database->error);
	}

 
    protected function __destruct()    
    {
        $this->database->close();
    }
    
    
    protected function generatePageHeader($headline = '') 
    {
        $headline = htmlspecialchars($headline);
        header("Content-type: text/html; charset=UTF-8");
      
       echo <<<EOT
       <!DOCTYPE html>
       <head>
       <html lang="de">
        <meta charset="utf-8">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link href="pizza.css" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <script src="pizza.js">

    </script>

    <title>$headline</title>
    </head>

    <body>

EOT;
    }

 
    protected function generatePageFooter() 
    {
       echo <<< EOT
       
       </body>
       </html>

EOT;
    }

    /**
     * Processes the data that comes via GET or POST i.e. CGI.
     * If every page is supposed to do something with submitted
     * data do it here. E.g. checking the settings of PHP that
     * influence passing the parameters (e.g. magic_quotes).
     *
     * @return none
     */
    protected function processReceivedData() 
    {
        if (get_magic_quotes_gpc()) {
            throw new Exception
                ("Bitte schalten Sie magic_quotes_gpc in php.ini aus!");
        }
    }
} // end of class

// Zend standard does not like closing php-tag!
// PHP doesn't require the closing tag (it is assumed when the file ends). 
// Not specifying the closing ? >  helps to prevent accidents 
// like additional whitespace which will cause session 
// initialization to fail ("headers already sent"). 
//? >
