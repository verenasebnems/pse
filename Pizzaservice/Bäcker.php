
<?php	
require_once './Page.php';


class Bäcker extends Page
{
    private $data;
    protected function __construct() 
    {
        parent::__construct();
        
    }
    
   
    protected function __destruct() 
    {
        parent::__destruct();
    }

    protected function getViewData()
    {
        
       $bestelltePizzen = array();
        $SQLabfrage = "SELECT * FROM bestelltepizza WHERE status < 3";
        
        $Recordset = $this->database->query($SQLabfrage);
        if(!$Recordset) { printf("Query failed: ", $this->_database->error); exit();
        }
        while($Record = $Recordset->fetch_assoc()) {
            $ID = $Record["PizzaID"];
            $Name = $Record["fPizzaName"];
            $Kunde = $Record["fBestellungID"];
            $Status = $Record["Status"];
            
            $bestelltePizzen[] = array('PizzaID' => $ID, 'fPizzaName' => $Name, 'fBestellungID' => $Kunde, 'Status' => $Status);
        }
        $Recordset->free();
        return $bestelltePizzen;
    }
    
    protected function generateView() 
    {
        $this->generatePageHeader('Bäcker');
        $bestelltePizzen = array();
        $bestelltePizzen = $this->getViewData();
        
        echo <<<EOT
        
            <div class="nav">
        <a class="col-lg-3 col-sm-6" href="Bestellung.php">Bestellen</a>
        <a class="col-lg-3 col-sm-6" href="Kunde.php">Kunden</a>
        <a class="col-lg-3 col-sm-6" href="B%C3%A4cker.php"><b>Bäcker</b></a>
        <a class="col-lg-3 col-sm-6" href="Fahrer.php">Fahrer</a>
    </div>
    
        <div class="flexcontainer col-lg-12">
EOT;
        foreach($bestelltePizzen as $Pizza) {
            $pID = $Pizza["PizzaID"];
            $Name = $Pizza["fPizzaName"];
            $Kunde = $Pizza["fBestellungID"];
            $Status = $Pizza["Status"];
            $value1id = "1" . $pID;
            $value2id = "2" . $pID;
            $value3id = "3" . $pID;
            
                if($Status == 1) {
            echo <<<EOT
            <form id = $pID method="post" name="radioupdate">
                            <div class="backeransicht2 col-lg-12">
                                <div>$Name ($Kunde)</div>
                                </div>
                                <div class="backeransicht3 col-lg-12">
                                <span><input type="radio" name="radio" value=$value1id onclick="postform($pID)" id=$pID checked = "checked"/> </span>
                                <span><input type="radio" name="radio" value=$value2id onclick="postform($pID)" id=$pID/></span>
                                <span><input type="radio" name="radio" value=$value3id onclick="postform($pID)" id=$pID/></span>
                            </div>
            </form>
EOT;

                } else if ($status == 2) {
            echo <<<EOT
            <form id = $pID method="post" name="radioupdate">
                            <div>
                                <div>$Name</div>
                                <span><input type="radio" name="radio" value=$value1id onclick="postform($pID)" id=$pID/> </span>
                                <span><input type="radio" name="radio" value=$value2id onclick="postform($pID)" id=$pID checked = "checked"/></span>
                                <span><input type="radio" name="radio" value=$value3id onclick="postform($pID)" id=$pID/></span>
                            </div>
            </form>
EOT;
                } else if ($status == 3) {

            echo <<<EOT
            <form id = $pID method="post" name="radioupdate">
                            <div>
                                <div>$Name</div>
                                <span><input type="radio" name="radio" value=$value1id onclick="postform($pID)" id=$pID/> </span>
                                <span><input type="radio" name="radio" value=$value2id onclick="postform($pID)" id=$pID/><span>
                                <span><input type="radio" name="radio" value=$value3id onclick="postform($pID)" id=$pID checked = "checked"/><span>
                            </div>
            </form>
EOT;

EOT;
     /*     echo<<<EOT
          
        <form id = $pID method="post" name="radioupdate">
            <tr>
               <td>$pname</td>
                <td><input type="radio" name="radio" value=$value1id onclick="postform($pID)" id=$pID checked = "checked"/> </td>
                <td><input type="radio" name="radio" value=$value2id onclick="postform($pID)" id=$pID/></td>
                <td><input type="radio" name="radio" value=$value3id onclick="postform($pID)" id=$pID/></td>
            </tr>
        </form>
            }
EOT;*/
  
            
        }
    }
    
    }

    protected function processReceivedData() 
    {
        parent::processReceivedData();
        
        if (isset($_POST['radio'])) {

             //   foreach($_POST as $item)
                    $item = $_POST['radio'];
                    $status = substr($item, 0, 1);
                    $pID = substr($item, 1, strlen($item)-1);

                    $sqlline = "UPDATE bestelltepizza SET Status = $status WHERE PizzaID = $pID";
                    $this->connection->query($sqlline);

    }
    }
    
    
    protected function update($value, $ID) {
        
           $Update = "UPDATE bestelltePizza SET status = 'value' WHERE PizzaID = 'ID'";
        
        console.log("test");
        
        $Recordset = $this->database->query($Update);
     
    }
    
    

     
    public static function main() 
    {
        try {
            $page = new Bäcker();
            $page->processReceivedData();
            $page->generateView();
            $sec = "5";
            //header("Refresh: $sec; url=Baecker.php");
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}


Bäcker::main();
