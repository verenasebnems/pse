<?php	

require_once './Page.php';

class Fahrer extends Page
{
    
    protected function __construct() 
    {
        parent::__construct();
        
    }
    
    protected function __destruct() 
    {
        parent::__destruct();
    }


    protected function getViewData()
    {
      $bestelltePizzen = array();
        $SQLabfrage = "SELECT * FROM bestelltepizza WHERE status >= 3";
        
        $Recordset = $this->database->query($SQLabfrage);
        if(!$Recordset) { printf("Query failed: ", $this->_database->error); exit();
        }
        while($Record = $Recordset->fetch_assoc()) {
            $ID = $Record["PizzaID"];
            $Name = $Record["fPizzaName"];
            $Kunde = $Record["fBestellungID"];
            $Status = $Record["Status"];
            
            $bestelltePizzen[] = array('PizzaID' => $ID, 'fPizzaName' => $Name, 'fBestellungID' => $Kunde, 'Status' => $Status);
        }
        $Recordset->free();
        return $bestelltePizzen;
    }
    

    protected function generateView() 
    {
        $this->generatePageHeader('Fahrer');
        $bestelltePizzen = array();
        $bestelltePizzen = $this->getViewData();
       
        
        echo <<<EOT
        <body>
   <div class="nav">
         <a class="col-lg-3 col-sm-6" href="Bestellung.php">Bestellen</a>
        <a class="col-lg-3 col-sm-6" href="Kunde.php">Kunden</a>
        <a class="col-lg-3 col-sm-6" href="B%C3%A4cker.php">Bäcker</a>
        <a class="col-lg-3 col-sm-6" href="Fahrer.php"><b>Fahrer</b></a>
    </div>

        <div class="flexcontainer col-lg-12">
        
EOT;
        foreach($bestelltePizzen as $Pizza) {
            $ID = $Pizza["PizzaID"];
            $Name = $Pizza["fPizzaName"];
            $Kunde = $Pizza["fBestellungID"];
            $Status = $Pizza["Status"];
            
            if($Status == 3){
                
                echo <<<EOT
  
                <div class="fahreransicht">
                Bestellung Nr. : $Kunde  - Pizza: $Name </br>
               <span> fertig </span>
               <span> unterwegs </span>
               <span> ausgeliefert </span>
               </div>

<div class="fahreransicht2">
<span> <input type="radio" name="$ID" value="1" id="fertigbaecker" onclick="update('1', $ID)" checked/></span>
<span> <input type="radio" name="$ID" value="2"
id="unterwegsfahrer" onclick="update('2', $ID)" /></span>
<span> <input type="radio" name="$ID" value="3"
id="ausgeliefertfahrer" onclick="update('3', $ID)" /></span>

</div>
EOT;
            }
      
                        if($Status == 4){
                echo <<<EOT
                Bestellung Nr. : $Kunde  - PizzaName: $Name; 
                <div class="fahreransicht">
               <span> fertig </span>
<span> unterwegs </span>
<span> ausgeliefert </span>
</div>

<div class="fahreransicht2">
<span> <input type="radio" name="$ID" value="1" id="fertigfahrer"/></span>
<span> <input type="radio" name="$ID" value="2"
id="unterwegsfahrer" checked/></span>
<span> <input type="radio" name="$ID" value="3"
id="ausgeliefertfahrer"/></span>
</div>
</div>

EOT;
            }
            

        $this->generatePageFooter();
    }
    
    }
    protected function processReceivedData() 
    {
        parent::processReceivedData();
    }

  
    public static function main() 
    {
        try {
            $page = new Fahrer();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}


Fahrer::main();


