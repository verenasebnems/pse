<?php	
require_once './Page.php';


class Kunde extends Page
{
  
    protected function __construct() 
    {
        parent::__construct();

    }
    

    protected function __destruct() 
    {
        parent::__destruct();
    }


    protected function getViewData()
    {
        $sql ="SELECT * FROM bestellung";
        
        $recordset = $this->database->query ($sql);
        if (!$recordset)
            throw new Exception ("Abfrage fehlgeschlafen: ".$this->database->error);
        
        $bestellung = array();
        $record = $recordset->fetch_assoc();
        
        while($record) {
            $bestellung[] = $record["BestellungID"];
            $record = $recordset->fetch_assoc();
        }
        $recordset->free();
        return $bestellung;
    }
    

    protected function generateView() 
    {
        $this->getViewData();
        $this->generatePageHeader('Kunde');
        
         
        echo <<<HERE
        
            <div class="nav">
        <a class="col-lg-3 col-sm-6" href="Bestellung.php">Bestellen</a>
        <a class="col-lg-3 col-sm-6" href="Kunde.php"><b>Kunden</b></a>
        <a class="col-lg-3 col-sm-6" href="B%C3%A4cker.php">Bäcker</a>
        <a class="col-lg-3 col-sm-6" href="Fahrer.php">Fahrer</a>
    </div>
    
        
    <div class="flexcontainer col-lg-12">
            
<div class="fahreransicht">
<span> bestellt </span>
<span> im Ofen </span>
<span> unterwegs </span>
</div>
        </div>

    <div class="flexcontainer col-lg-12">
<div class="fahreransicht2">
<span> <input type="radio" name="bestellt" value="1" id="bestelltKunde"/> </span>
<span> <input type="radio" name="imofen" value="1"
id="imofenKunde"/></span>
<span> <input type="radio" name="unterwegs" value="1"
id="unterwegsKunde"/> </span>
</div>
        
    </div>
    
HERE;
        
        $this->generatePageFooter();
    }
    

    protected function processReceivedData() 
    {
        parent::processReceivedData();
    
    }

   
    public static function main() 
    {
        try {
            $page = new Kunde();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}


Kunde::main();

