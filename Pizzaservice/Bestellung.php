<?php	

require_once './Page.php';

class Bestellung extends Page
{
    private $selectedPizza;

    
    protected function __construct() 
    {
        parent::__construct();
        session_start();

    }
    

    protected function __destruct() 
    {
        parent::__destruct();
        session_destroy();
    }

   
    protected function getViewData()
    {
         
        $sql ="SELECT * FROM angebot ORDER by Preis";
        
        $recordset = $this->database->query ($sql);
        if (!$recordset)
            throw new Exception ("Abfrage fehlgeschlafen: ".$this->database->error);
        
        $angebot = array();
        $record = $recordset->fetch_assoc();
        
        while($record) {
            $angebot[] = $record["PizzaName"];
            $record = $recordset->fetch_assoc();
        }
        $recordset->free();
        
        return $angebot;
        // to do: fetch data for this view from the database
    }
    
    private function price() {
        
        $sql ="SELECT * FROM angebot ORDER by Preis";
        
        $recordset = $this->database->query ($sql);
        if (!$recordset)
            throw new Exception ("Abfrage fehlgeschlafen: ".$this->database->error);
        
        $angebot = array();
        $record = $recordset->fetch_assoc();
        
        while($record) {
            $angebot[] = $record["Preis"];
            $record = $recordset->fetch_assoc();
        }
        $recordset->free();
        
        return $angebot;
        
    }
    
        private function picture() {
        
        $sql ="SELECT * FROM angebot ORDER by Preis";
        
        $recordset = $this->database->query ($sql);
        if (!$recordset)
            throw new Exception ("Abfrage fehlgeschlafen: ".$this->database->error);
        
        $angebot = array();
        $record = $recordset->fetch_assoc();
        
        while($record) {
            $angebot[] = $record["Bilddatei"];
            $record = $recordset->fetch_assoc();
        }
        $recordset->free();
        
        return $angebot;
        
    }

    protected function generateView() 
    {
        $name = $this->getViewData();
        $price = $this->price();
        $picture = $this->picture();
        
        $numOfRecords = count($name);
        
    
        $this->generatePageHeader('Bestellung');   
        
        echo <<<HERE
        
            <div class="nav">
        <a class="col-lg-3 col-sm-6" href="Bestellung.php"><b>Bestellen</b></a>
        <a class="col-lg-3 col-sm-6" href="Kunde.php">Kunden</a>
        <a class="col-lg-3 col-sm-6" href="B%C3%A4cker.php">Bäcker</a>
        <a class="col-lg-3 col-sm-6" href="Fahrer.php">Fahrer</a>
    </div>

    <div class="flexcontainer">
    
        <div class="auswahl col-lg-12">
            <div class="pizza col-lg-4 col-md-6 col-sm-12">
                <p>$name[0] - $price[0] €</p>
                <img src= $picture[0] width="60%;" height="auto" alt="Bild von Pizza" id="margherita" title="salami" onclick="addmargherita();checkorder()" data-preis=$price[0] />
            </div>

            <div class="pizza col-lg-4 col-md-6 col-sm-12">
               <p>$name[1] - $price[1] €</p>
                <img src= $picture[1] width="60%;" height="auto" alt="Bild von Pizza" id="salami" title="salami" onclick="addsalami();checkorder()" data-preis=$price[1] />
            </div>

            <div class="pizza col-lg-4 col-md-6 col-sm-12">
                <p>$name[2] - $price[2] €</p>
                <img src= $picture[2] width="60%;" height="auto" alt="Bild von Pizza" id="hawaii" title="hawaii" onclick="addhawaii();checkorder()" data-preis=$price[2] />
            </div>
        </div>
HERE;
        include("cart.html");

    echo "</div>";
     
        // to do: call generateView() for all members
        // to do: output view of this page
        $this->generatePageFooter();
    }
    
    
    protected function processReceivedData() 
    {
        parent::processReceivedData();
        // to do: call processReceivedData() for all members
        if(isset($_POST["bestellung"])){
            $name = $_POST["nachname"];
            $vorname = $_POST["vorname"];
            $strasse = $_POST["strasse"];
            $ort = $_POST["ort"];
            
            $kundendaten = $vorname . " " . $name . " " . $strasse . " " . $ort;
            
            $sql = "INSERT INTO bestellung (BestellungID, Adresse, Bestellzeitpunkt) VALUES ('', '$kundendaten', ' ')";
            
            if($this->database->query($sql)){
                $ID = $this->database->insert_id;
                $orderedPizza = $_POST["bestellung"];
                foreach($orderedPizza as $PizzaID ){
                    
                    $sqlholen = "SELECT PizzaName from angebot WHERE PizzaID = '$PizzaID'";
                    $recordset = $this->database->query ($sqlholen);
                    if (!$recordset)
                        throw new Exception ("Abfrage fehlgeschlafen: ".$this->database->error);
        
                    $record = $recordset->fetch_assoc();
        
                    while($record) {
                    $angebot = $record["PizzaName"];
                    $record = $recordset->fetch_assoc();
                    
            }
            $recordset->free();
                    
            $sqlInfo = "INSERT INTO bestelltepizza(PizzaID, fBestellungID, fPizzaName, Status) VALUES ('', $ID , '$angebot' , 1)";
                    
            $this->database->query($sqlInfo);
            
                }
            }
        }
    }

    public static function main() 
    {
        try {
            $page = new Bestellung();
            $page->processReceivedData();
            $page->generateView();
            
            }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}


 Bestellung::main();
