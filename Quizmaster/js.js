// Globale Variablen
var richtig;
var xhr = new XMLHttpRequest();

// Global AJAX response handler
xhr.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
        var response = JSON.parse(this.responseText);
        processJSONResponse(response);
    } else if (this.status !== 200) {
        //alert("AJAX Request failed");
    }
};

// Erzeugt einen AJAX-GET-Request (siehe 4b)
function  getQuestion() {
    "use strict";
    xhr.open("GET", "question_provider.php", true);
    xhr.send();
}

function processJSONResponse(data) {
    "use strict"
    richtig = data.richtig;
    addQuestionToDOM(data);
    createAnswerButtons(data);
}

// bereits implementiert
function addQuestionToDOM(data) {
    "use strict";
    var container = document.getElementById("frage");
    var frage = document.createTextNode(data["frage"]);
    container.appendChild(frage);

}

// Erzeugt die Antwort-Buttons (siehe c4)
function createAnswerButtons(data) {
    "use strict";
    var container = document.getElementById("antworten");
    for (var i = 0; i < 3; i++) {
        var button = document.createElement("button");
        var text = document.createTextNode(data["antwort" + (i+1)]);
        button.dataset.num = i+1;
        button.onclick = checkAnswer;
        button.appendChild(text);
        container.appendChild(button);
    }
}

// onClick-Handler für Antwort-Buttons (siehe 4d)
function checkAnswer() {
    var button = event.target;
    button.classList.add("selected");
    var parent = button.parentNode;
    for (i = 0; i < parent.childElementCount; i++) {
        var child = parent.childNodes[i];
        if (child.dataset.num === richtig) {
            child.classList.add("correct");
        } else {
            child.classList.add("wrong");
        }
    }
}