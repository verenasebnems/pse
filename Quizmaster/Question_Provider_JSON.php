<?php
	
class Frage {
	public $frage;
	public $antwort1;
	public $antwort2;
	public $antwort3;
	public $richtig;
}

// Start kritischer Codeabschnitt
try {

// MIME-Type der Antwort definieren
    header("Content-type:application/json");

// Datenbank öffnen und abfragen
    $host = "localhost";
    $user =  "verena";
    $pwd = "geheimnis";
    $db = "quiz";
    $Connection = new MySQLi($host, $user, $pwd, $db);

// Verbindung überprüfen
    if  ($Connection->connect_error) {
        die("Connection failed: " . $Connection->connect_error);
    }

// SQL-Abfrage festlegen
    $sql = "SELECT DISTINCT * FROM question ORDER BY rand() LIMIT 1";
    $result = $Connection->query($sql);
    if ($result->num_rows < 0) {
        die("Error: Database query failed.");
    }

// Erzeugung eines neuen Frage-Objekts und Speicherung der Daten aus dem Recordset
    $row = $result->fetch_assoc();
    $frage = new Frage();
    $frage->frage = htmlspecialchars($row['frage']);
    $frage->antwort1 = htmlspecialchars($row['antwort1']);
    $frage->antwort2 = htmlspecialchars($row['antwort2']);
    $frage->antwort3 = htmlspecialchars($row['antwort3']);
    $frage->richtig = htmlspecialchars($row['richtig']);

// Serialisierung der Frage-Instanz in einem JSON-String und Ausgabe
    $json = json_encode($frage);
    echo $json;

// Ende kritischer Codeabschnitt
} catch (Exception $e) {
    echo $e->getMessage();
}