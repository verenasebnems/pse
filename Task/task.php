<?php
abstract class Page
{
 protected $database = null;
 protected function __construct() {
 	$this->database = new MySQLi("localhost", "lucas", "dergraf1", "tasks_db");
 }

 protected function __destruct() {
 	$this->database->close();
 }

 protected function generatePageHeader($title = '') {
 	$title = htmlspecialchars($title);
 	header("Content-type: text/html; charset=UTF-8");
 	echo <<<HTML
 <!DOCTYPE html>
<html lang="de">
<head>
<link rel="stylesheet" type="text/css" href="task.css" media="screen" />
<script src="js.js"></script>
 <meta charset="UTF-8"/>
 <title>$title</title>
 </head>
<body>
HTML;
 }

 protected function generatePageFooter() {
 	echo <<<HTML
 </body>
</html>
HTML;
 }
}


class Task extends Page
{
    protected $id = array();
    protected $desc = array();
    protected $due = array();
    protected $prio = array();




 protected function __construct() 
    {
        parent::__construct();
    }
    
    protected function __destruct()
    {
        parent::__destruct();
    }

    protected function getViewData()
    {
        $sql_query = "SELECT id, description, due, prio FROM tbl_tasks";
        $result = $this->database->query($sql_query);

        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()) {
                array_push($this->id, $row["id"]);
                array_push($this->desc,  htmlspecialchars($row["description"]));
                array_push($this->due,  htmlspecialchars($row["due"]));
                array_push($this->prio,  htmlspecialchars($row["prio"]));
            }
        }
    }

    protected function generateView()
    {
        $this->getViewData();
        $this->generatePageHeader('Taskmanager 2000');

        

        echo<<<HTML
        <section>
            <h2>Taskliste:</h2>
            <div class="appendingTasks">
                <select id="listTasks" multiple>
HTML;
                $arrLen = count($this->id);
                for($i = 0 ; $i < $arrLen ; $i++){

                	if($this->prio[$i] == 1){

                		 echo <<<HTML
                    <option class="prio1" value="{$this->id[$i]}">{$this->due[$i]} - {$this->desc[$i]} (Prio:{$this->prio[$i]})</option>
HTML;

                	}
                   else{

                   	 echo <<<HTML
                    <option value="{$this->id[$i]}">{$this->due[$i]} - {$this->desc[$i]} (Prio:{$this->prio[$i]})</option>
HTML;

                   }
                   
               

                }

                echo<<<HTML
                </select>
                <button type="button" onclick="taskUp()">Up</button>
                <button type="button" name="down" onclick="taskDown()">Down</button> 
            </div>
        </section>
        <article class="newTask">
            <form action="task.php" method="post" >
                <label><input type="text" name="beschreibung" placeholder="Beschreibung">Beschreibung</label>
                
                <label>Zu erledigen bis<input type="date" name="datum"></label>
                
                <label><input value="1" type="radio" name="prio" checked>1</label>
                <label><input value="2" type="radio" name="prio">2</label>
                <label><input value="3" type="radio" name="prio">3</label>       
                              
                <input type="submit" name="Hinzufügen">
                
            </form>
            
        </article>

HTML;


        $this->generatePageFooter();
    }

    protected function processReceivedData() 
    {
        if(isset($_POST['beschreibung'])){
             $taskDescription = $this->database->real_escape_string( $_POST['beschreibung']);
        $taskDate = $this->database->real_escape_string( $_POST['datum']);
        $taskPrio = $this->database->real_escape_string( $_POST['prio']);
        


        $insertTask = "INSERT INTO tbl_tasks(description, due, prio) VALUES ('$taskDescription', '$taskDate','$taskPrio' )";
        $this->database->query($insertTask);
        
        }
    }






 public static function main() {
 try {
 $page = new Task();
 $page->processReceivedData();
 $page->generateView();
 }
 catch (Exception $e) {
 header("Content-type: text/plain; charset=UTF-8");
 echo $e->getMessage();
 }
 }
}
Task::main();



