function taskUp(){

	var selIndex = document.getElementById("listTasks").selectedIndex;
	if(selIndex < 0){

		alert("kein Item ausgewaehlt");
		return;

	}

	if(selIndex > 0 ){
		var onTopOption = selIndex - 1;
	}
	else{
		alert("oberstes Element ausgewaehlt");
		return;
	}


	var tasks = document.getElementById("listTasks").options;

	var selIndTextTmp = tasks[selIndex];
	var topIndTextTmp = tasks[onTopOption];

	tasks.remove(selIndex);
	tasks.remove(onTopOption);

	var x = document.createElement("option");
	var y = document.createElement("option");

	if(selIndTextTmp.className === "prio1"){
		x.text = selIndTextTmp.text;

		tasks.add(x,onTopOption);
		x.className = "prio1";
	}
	else{
		x.text = selIndTextTmp.text;
		tasks.add(x,onTopOption);
	}

	if(topIndTextTmp.className === "prio1"){
		y.text = topIndTextTmp.text;
		y.className = "prio1";
		tasks.add(y, selIndex);

	}
	else{
		y.text = topIndTextTmp.text;
		tasks.add(y, selIndex);
	}
}



function taskDown(){

	var selIndex = document.getElementById("listTasks").selectedIndex;

	if(selIndex < 0){
		alert("kein Item ausgewaehlt");
		return;
	}

	if(selIndex < document.getElementById("listTasks").length -1){
		var underSelItem = selIndex + 1;
	}
	else{
		alert("unterstes Element ausgewaehlt");
		return;
	}

	var tasks = document.getElementById("listTasks").options;

	var selIndTextTmp = tasks[selIndex];


	tasks.remove(selIndex);
	//nur ein remove, das alle elemente unter dem removten item hochrutschen
	//(also das untere schon in gewünschter position ist)
	//man muss nun nur noch eine neue option adden

	var x = document.createElement("option");

	if(selIndTextTmp.className === "prio1"){

		x.text = selIndTextTmp.text;
		x.className = "prio1";
		tasks.add(x,underSelItem);

	}
	else{

		x.text = selIndTextTmp.text;
		tasks.add(x,underSelItem);
	}
}

